// Created with Motiva Layama v1.6 https://www.motivacg.com/layama

function getLayamaCameras()
{
   var layamaCameras = new BABYLON.SmartArray(0);
   layamaCameras.push({n: "vt0000", a: "Corona_Camera001", p: new BABYLON.Vector3(-2577.84, -1411.03, 921.48), l: new BABYLON.Vector3(-2577.88, -1411.03, 921.571)});
   layamaCameras.push({n: "vt0001", a: "Corona_Camera002", p: new BABYLON.Vector3(-2577.84, -1411.03, 1125.83), l: new BABYLON.Vector3(-2577.84, -1411.03, 1125.93)});
   layamaCameras.push({n: "vt0002", a: "Corona_Camera003", p: new BABYLON.Vector3(-2888.61, -1411.03, 1125.83), l: new BABYLON.Vector3(-2888.69, -1411.03, 1125.78)});
   layamaCameras.push({n: "vt0003", a: "Corona_Camera004", p: new BABYLON.Vector3(-3186.52, -1411.03, 1125.83), l: new BABYLON.Vector3(-3186.6, -1411.03, 1125.78)});
   layamaCameras.push({n: "vt0004", a: "Corona_Camera005", p: new BABYLON.Vector3(-2577.84, -1411.03, 1398.42), l: new BABYLON.Vector3(-2577.74, -1411.03, 1398.42)});
   layamaCameras.push({n: "vt0005", a: "Corona_Camera006", p: new BABYLON.Vector3(-2583.88, -1411.03, 1581.49), l: new BABYLON.Vector3(-2583.98, -1411.03, 1581.48)});
   layamaCameras.push({n: "vt0006", a: "Corona_Camera007", p: new BABYLON.Vector3(-3223.16, -1411.03, 1556.71), l: new BABYLON.Vector3(-3223.06, -1411.03, 1556.71)});
   layamaCameras.push({n: "vt0007", a: "Corona_Camera008", p: new BABYLON.Vector3(-2577.22, -1411.03, 1683.11), l: new BABYLON.Vector3(-2577.31, -1411.03, 1683.13)});
   layamaCameras.push({n: "vt0008", a: "Corona_Camera009", p: new BABYLON.Vector3(-2749.73, -1411.03, 1712.66), l: new BABYLON.Vector3(-2749.83, -1411.03, 1712.67)});
   layamaCameras.push({n: "vt0009", a: "Corona_Camera010", p: new BABYLON.Vector3(-3065.98, -1411.03, 1712.66), l: new BABYLON.Vector3(-3066.07, -1411.03, 1712.67)});
   layamaCameras.push({n: "vt0010", a: "Corona_Camera011", p: new BABYLON.Vector3(-2441.95, -1411.03, 1466.48), l: new BABYLON.Vector3(-2441.85, -1411.03, 1466.48)});
   layamaCameras.push({n: "vt0011", a: "Corona_Camera012", p: new BABYLON.Vector3(-2577.84, -1411.03, 693.254), l: new BABYLON.Vector3(-2577.88, -1411.03, 693.345)});
   layamaCameras.push({n: "vt0012", a: "Corona_Camera013", p: new BABYLON.Vector3(-2684.11, -1411.03, 1829.71), l: new BABYLON.Vector3(-2684.03, -1411.03, 1829.77)});
   return layamaCameras;
}

function getLayamaResolutions()
{
   var layamaResolutions = new BABYLON.SmartArray(0);
   layamaResolutions.push("1024");
   layamaResolutions.push("1024");
   return layamaResolutions;
}

function getLayamaControls()
{
   return {defMove: false, defRot: 1, altMove: true, altRot: 2};
}

